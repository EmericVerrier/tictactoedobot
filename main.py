import sys 
from PyQt5 import QtWidgets
import sources.View.mainGUI
from sources.Controller.AppController import AppController

app = QtWidgets.QApplication(sys.argv)
mainView = sources.View.mainGUI.Ui_MainWindow(appController=AppController(app))
mainView.MainWindow.show()
sys.exit(app.exec_())