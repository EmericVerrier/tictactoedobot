from PyQt5.QtWidgets import *
import PyQt5.QtMultimedia
from PyQt5.Qt import QEvent
import PyQt5.QtMultimediaWidgets
import PyQt5.QtCore
import sources.Controller.AppController
import sys
import os

""" 
Cette vue doit permettre de gérer plus précisément les paramètres appliqués à la reconnaissance d'image et à la caméra utilisée
pour permettre ainsi d'obtenir de meilleurs résultats lors de la reconnaissance d'image
"""
class calibrationGUI(QMainWindow):
    def __init__(self,appController):
        super().__init__()

        self.appController = appController
        self.camera = PyQt5.QtMultimedia.QCamera()
        self.fenetreCam = PyQt5.QtMultimediaWidgets.QCameraViewfinder(self)
        self.capture = PyQt5.QtMultimedia.QCameraImageCapture(self.camera)
        self.fenetreCam.setMinimumSize(640,480)
        self.camera.setCaptureMode(PyQt5.QtMultimedia.QCamera.CaptureStillImage)
        self.camera.setViewfinder(self.fenetreCam)
        if self.camera.isAvailable():
            self.capture.setCaptureDestination(PyQt5.QtMultimedia.QCameraImageCapture.CaptureToBuffer)
            self.camera.start()

        self.camera.setViewfinder(self.fenetreCam)
        self.setCentralWidget(QWidget())
        self.mainLayout = QBoxLayout(QBoxLayout.TopToBottom, parent=self.centralWidget())
        self.centralWidget().setLayout(self.mainLayout)
        self.QCamViewer = PyQt5.QtMultimediaWidgets.QCameraViewfinder()
        self.cameraSelector = QComboBox()
        self.cameraSelector.setToolTip("Select a camera")
        self.availableCamera = PyQt5.QtMultimedia.QCameraInfo.availableCameras()
        for cam in self.availableCamera:
            self.cameraSelector.addItem(cam.description(),userData=cam)
        self.cameraSelector.currentIndexChanged.connect(self.currentIndexChanged)
        self.cameraSelector.activated.connect(self.currentIndexChanged)
        self.sliderPionAbscisse = QSlider(PyQt5.QtCore.Qt.Horizontal,self.centralWidget())
        self.sliderPionOrdonnee = QSlider(PyQt5.QtCore.Qt.Vertical,self.centralWidget())

        self.sliderBlurIntensity = QSlider(PyQt5.QtCore.Qt.Horizontal,self.centralWidget())
        self.sliderBlurIntensity.setTickPosition(QSlider.TicksBelow)
        self.sliderBlurIntensity.setFixedHeight(20)
        self.sliderBlurIntensity.setFixedWidth(self.centralWidget().geometry().width()-40)
        self.sliderBlurIntensity.setMaximum(20)
        self.sliderCannyEdge = QSlider(PyQt5.QtCore.Qt.Horizontal,parent=self.centralWidget())
        self.sliderCannyEdge.setTickPosition(QSlider.TicksBelow)
        self.sliderCannyEdge.setFixedHeight(self.sliderBlurIntensity.height())
        self.sliderCannyEdge.setFixedWidth(self.sliderBlurIntensity.width())
        self.sliderCannyEdge.setMaximum(20)
        self.sliderRoundApertureCamera = PyQt5.QtWidgets.QDial(self)
        self.sliderEpsilonApproximation = QSlider(PyQt5.QtCore.Qt.Horizontal,self.centralWidget())
        self.sliderEpsilonApproximation.setTickPosition(QSlider.TicksBelow)
        self.sliderEpsilonApproximation.setFixedWidth(self.sliderBlurIntensity.width())
        self.sliderEpsilonApproximation.setFixedHeight(self.sliderBlurIntensity.height())
        self.sliderEpsilonApproximation.setMaximum(20)
        
        self.labelBlurIntensity = QLabel("Blur level")
        self.labelBlurIntensity.setBuddy(self.sliderBlurIntensity)
        self.labelCannyEdge = QLabel("CannyEdge Thresholding")
        self.labelCannyEdge.setBuddy(self.sliderCannyEdge)
        self.labelEpsilonApproximation = QLabel("Epsilon level")
        self.labelEpsilonApproximation.setBuddy(self.sliderEpsilonApproximation)

        self.buttonPreview = QPushButton("Preview",parent=self)
        self.buttonPreview.setGeometry(200, 300, 20, 30)
        self.buttonPreview.pressed.connect(self.buttonPreviewPressed)
        self.buttonPreview.released.connect(self.buttonPreviewReleased)
        self.centralWidget().layout().addWidget(self.cameraSelector)
        self.centralWidget().layout().addWidget(self.labelBlurIntensity)
        self.centralWidget().layout().addWidget(self.sliderBlurIntensity)
        self.centralWidget().layout().addWidget(self.labelCannyEdge)
        self.centralWidget().layout().addWidget(self.sliderCannyEdge)
        self.centralWidget().layout().addWidget(self.labelEpsilonApproximation)
        self.centralWidget().layout().addWidget(self.sliderEpsilonApproximation)
        self.centralWidget().layout().addWidget(self.buttonPreview)
        self.centralWidget().layout().addWidget(self.fenetreCam)

        self.HorizontalLayout = PyQt5.Qt.QHBoxLayout()

        self.labelSliderPionAbscisse = QLabel(text="PositionX",parent=self)
        self.labelSliderPionOrdonnee = QLabel(text="PositionY",parent=self)
        self.labelRoundApertureCamera = QLabel(text="Angle",parent=self)

        self.labelSliderPionAbscisse.setBuddy(self.sliderPionAbscisse)
        self.labelSliderPionOrdonnee.setBuddy(self.sliderPionOrdonnee)
        self.labelRoundApertureCamera.setBuddy(self.sliderRoundApertureCamera)
        
        self.HorizontalLayout.addWidget(self.sliderPionAbscisse)
        self.HorizontalLayout.addWidget(self.labelSliderPionAbscisse)
        self.HorizontalLayout.addWidget(self.sliderPionOrdonnee)
        self.HorizontalLayout.addWidget(self.labelSliderPionOrdonnee)
        self.HorizontalLayout.addWidget(self.sliderRoundApertureCamera)
        self.HorizontalLayout.addWidget(self.labelRoundApertureCamera)
        self.mainLayout.setAlignment(PyQt5.QtCore.Qt.AlignCenter)
        self.mainLayout.addItem(self.HorizontalLayout)
        self.show()
    def buttonPreviewPressed(self):
        self.capture.capture()
        #self.capture.imageAvailable(id, image)
        print(self.cameraSelector.currentText())
    def buttonPreviewReleased(self):
        pass
    
    # def showEvent(self, a0):
    #     self.currentIndexChanged()
    def currentIndexChanged(self):
        camSelectedInfo = PyQt5.QtMultimedia.QCameraInfo(self.cameraSelector.currentData())
        self.camera = PyQt5.QtMultimedia.QCamera(camSelectedInfo)
        self.camera.setViewfinder(self.fenetreCam)
        if self.camera.isAvailable():
            self.camera.start()
        self.camera.setCaptureMode(PyQt5.QtMultimedia.QCamera.CaptureStillImage)
        
    def manageError(self):
        print("erreur dans l'ouverture de la caméra")
    def closeEvent(self, a0):
        self.camera.stop()
        a0.accept()