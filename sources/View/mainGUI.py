# -*- coding: utf-8 -*-

import sources.Controller.AppController
from PyQt5 import QtCore, QtGui, QtWidgets
import sources.Model.MyDobotControl

class Ui_MainWindow(object):
    def __init__(self,appController):
        self.AppController = appController
        self.MainWindow = QtWidgets.QMainWindow()
        self.dobot = sources.Model.MyDobotControl.MyDobot()
        self.MainWindow.setObjectName("MainWindow")
        self.MainWindow.resize(1090, 767)
        self.MainWindow.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("plateauSimple.jpg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.MainWindow.setWindowIcon(icon)
        self.MainWindow.setStyleSheet("background-color: rgb(0, 85, 127);")
        self.centralwidget = QtWidgets.QWidget(self.MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.btnPlay = QtWidgets.QPushButton(self.centralwidget)
        self.btnPlay.setGeometry(QtCore.QRect(370, 490, 341, 131))
        font = QtGui.QFont()
        font.setFamily("Bahnschrift Light Condensed")
        font.setPointSize(20)
        font.setBold(True)
        font.setWeight(75)
        self.btnPlay.setFont(font)
        self.btnPlay.setAutoFillBackground(False)
        self.btnPlay.setStyleSheet("color: #333;\n"
"    border: 2px solid #555;\n"
"    border-radius: 50px;\n"
"    border-style: outset;\n"
"    background: qradialgradient(\n"
"        cx: 0.3, cy: -0.4, fx: 0.3, fy: -0.4,\n"
"        radius: 1.35, stop: 0 #fff, stop: 1 #888\n"
"        );\n"
"    padding: 5px;")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("unnamed.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btnPlay.setIcon(icon1)
        self.btnPlay.setIconSize(QtCore.QSize(50, 50))
        self.btnPlay.setFlat(False)
        self.btnPlay.setObjectName("btnPlay")
        self.logo = QtWidgets.QLabel(self.centralwidget)
        self.logo.setGeometry(QtCore.QRect(370, 170, 321, 271))
        self.logo.setAutoFillBackground(False)
        self.logo.setText("")
        self.logo.setPixmap(QtGui.QPixmap("RobotLAB Dobot Robotic Arm-1-2-3.png"))
        self.logo.setScaledContents(True)
        self.logo.setObjectName("logo")
        self.lbhome = QtWidgets.QLabel(self.centralwidget)
        self.lbhome.move(370, 50)
        font = QtGui.QFont()
        font.setFamily("PMingLiU-ExtB")
        font.setPointSize(26)
        font.setBold(True)
        font.setWeight(75)
        self.lbhome.setFont(font)
        self.lbhome.setAutoFillBackground(False)
        self.lbhome.setStyleSheet("color: rgb(255, 255, 255);")
        self.lbhome.setObjectName("lbhome")
        self.btnMaint = QtWidgets.QPushButton(self.centralwidget)
        self.btnMaint.setGeometry(QtCore.QRect(880, 640, 181, 101))
        font = QtGui.QFont()
        font.setFamily("Bahnschrift Light Condensed")
        font.setPointSize(16)
        font.setBold(True)
        font.setWeight(75)
        self.btnMaint.setFont(font)
        self.btnMaint.setAutoFillBackground(False)
        self.btnMaint.setStyleSheet("color: #333;\n"
"    border: 2px solid #555;\n"
"    border-radius: 50px;\n"
"    border-style: outset;\n"
"    background: qradialgradient(\n"
"        cx: 0.3, cy: -0.4, fx: 0.3, fy: -0.4,\n"
"        radius: 1.35, stop: 0 #fff, stop: 1 #888\n"
"        );\n"
"    padding: 5px;")
        self.btnMaint.setIconSize(QtCore.QSize(50, 50))
        self.btnMaint.setFlat(False)
        self.btnMaint.setObjectName("btnMaint")
        self.MainWindow.setCentralWidget(self.centralwidget)
        self.retranslateUi(self.MainWindow)
        self.btnPlay.clicked.connect(self.AppController.btnPlay_click)
        self.btnMaint.clicked.connect(self.AppController.btnMaint_click)
        QtCore.QMetaObject.connectSlotsByName(self.MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        self.MainWindow.setWindowTitle(_translate("MainWindow", "TIC TAC TOE"))
        self.btnPlay.setText(_translate("MainWindow", "PLAY"))
        self.lbhome.setText(_translate("MainWindow", "TIC TAC TOE "))
        self.btnMaint.setText(_translate("MainWindow", "Maintenance"))

    def btnPlay_click(self):
        self.MWindow = QtWidgets.QMainWindow()
        self.ui=Ui_level(self.keepingWindows)
        self.ui.setupUi(self.MWindow)
        self.MWindow.show()

    def btnMaint_click(self):
        self.ui=Ui_maintenance()
        self.ui.maintenanceMainWindow.show()
        
if __name__ == "__main__":
    import sys
    import sources
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())