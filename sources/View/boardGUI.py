# -*- coding: utf-8 -*-

from PyQt5 import QtCore, QtGui, QtWidgets
import sys
import random
from time import sleep
import PIL.Image, PIL.ImageFont, PIL.ImageDraw
from random import randint
from sources.Model import ImageRecognition, GameBoard

class Ui_board():
    def __init__(self,gameController):
        self.gameController = gameController
        self.boardMainWindow = QtWidgets.QMainWindow()
        self.dfStyle=("color: #333;\n"
        "    border: 2px solid #555;\n"
        "    border-radius: 50px;\n"
        "    border-style: outset;\n"
        "    background: qradialgradient(\n"
        "        cx: 0.3, cy: -0.4, fx: 0.3, fy: -0.4,\n"
        "        radius: 1.35, stop: 0 #fff, stop: 1 #888\n"
        "        );\n"
        "    padding: 5px;")

        self.boardMainWindow.setObjectName("level")
        self.boardMainWindow.setFixedSize(QtWidgets.QApplication.desktop().width(), QtWidgets.QApplication.desktop().height())
        self.boardMainWindow.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("plateauSimple.jpg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.boardMainWindow.setWindowIcon(icon)
        self.boardMainWindow.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.boardMainWindow.setStyleSheet("background-color: rgb(0, 85, 127);")
        self.centralwidget = QtWidgets.QWidget(self.boardMainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.logo = QtWidgets.QLabel(self.centralwidget)
        self.logo.setGeometry(QtCore.QRect(20, 300, 491, 501))
        self.logo.setMaximumSize(QtCore.QSize(5000, 5000))
        self.logo.setText("")
        self.logo.setPixmap(QtGui.QPixmap("pb.png"))
        self.logo.setScaledContents(True)
        self.logo.setObjectName("logo")
    
        font = QtGui.QFont()
        font.setFamily("PMingLiU-ExtB")
        font.setPointSize(26)
        font.setBold(True)
        font.setWeight(75)
        
        font = QtGui.QFont()
        font.setFamily("PMingLiU-ExtB")
        font.setPointSize(26)
        font.setBold(True)
        font.setWeight(75)
        
        font = QtGui.QFont()
        font.setFamily("PMingLiU-ExtB")
        font.setPointSize(26)
        font.setBold(True)
        font.setWeight(75)
        
        self.lbscore_2 = QtWidgets.QLabel(self.centralwidget)
        self.lbscore_2.move(730, 20)
        font = QtGui.QFont()
        font.setFamily("PMingLiU-ExtB")
        font.setPointSize(26)
        font.setBold(True)
        font.setWeight(75)
        self.lbscore_2.setFont(font)
        self.lbscore_2.setStyleSheet("color: rgb(255, 255, 255);")
        self.lbscore_2.setObjectName("lbscore_2")
        self.btnplat3 = QtWidgets.QPushButton(self.centralwidget)
        self.btnplat3.setGeometry(QtCore.QRect(350, 360, 111, 101))
        self.btnplat3.setStyleSheet(self.dfStyle)
        self.btnplat3.setText("")
        self.btnplat3.setObjectName("btnplat3")
        self.btnplat2 = QtWidgets.QPushButton(self.centralwidget)
        self.btnplat2.setGeometry(QtCore.QRect(210, 360, 111, 101))
        self.btnplat2.setStyleSheet(self.dfStyle)
        self.btnplat2.setText("")
        self.btnplat2.setObjectName("btnplat2")
        self.btnplat1 = QtWidgets.QPushButton(self.centralwidget)
        self.btnplat1.setGeometry(QtCore.QRect(70, 360, 111, 101))
        self.btnplat1.setStyleSheet(self.dfStyle)
        self.btnplat1.setText((""))
        self.btnplat1.setObjectName(("btnplat1"))
        self.btnplat4 = QtWidgets.QPushButton(self.centralwidget)
        self.btnplat4.setGeometry(QtCore.QRect(70, 500, 111, 101))
        self.btnplat4.setStyleSheet(self.dfStyle)
        self.btnplat4.setText((""))
        self.btnplat4.setObjectName(("btnplat4"))
        self.btnplat5 = QtWidgets.QPushButton(self.centralwidget)
        self.btnplat5.setGeometry(QtCore.QRect(210, 500, 111, 101))
        self.btnplat5.setStyleSheet(self.dfStyle)
        self.btnplat5.setText((""))
        self.btnplat5.setObjectName(("btnplat5"))
        self.btnplat6 = QtWidgets.QPushButton(self.centralwidget)
        self.btnplat6.setGeometry(QtCore.QRect(350, 500, 111, 101))
        self.btnplat6.setStyleSheet(self.dfStyle)
        self.btnplat6.setText((""))
        self.btnplat6.setObjectName(("btnplat6"))
        self.btnplat7 = QtWidgets.QPushButton(self.centralwidget)
        self.btnplat7.setGeometry(QtCore.QRect(70, 640, 111, 101))
        self.btnplat7.setStyleSheet(self.dfStyle)
        self.btnplat7.setText((""))
        self.btnplat7.setObjectName(("btnplat7"))
        self.btnplat8 = QtWidgets.QPushButton(self.centralwidget)
        self.btnplat8.setGeometry(QtCore.QRect(210, 640, 111, 101))
        self.btnplat8.setStyleSheet(self.dfStyle)
        self.btnplat8.setText((""))
        self.btnplat8.setObjectName(("btnplat8"))
        self.btnplat9 = QtWidgets.QPushButton(self.centralwidget)
        self.btnplat9.setGeometry(QtCore.QRect(350, 640, 111, 101))
        self.btnplat9.setStyleSheet(self.dfStyle)
        self.btnplat9.setText((""))
        self.btnplat9.setObjectName(("btnplat9"))
        self.btnValid = QtWidgets.QPushButton(self.centralwidget)
        self.btnValid.setGeometry(QtCore.QRect(180, 830, 141, 111))
        font = QtGui.QFont()
        font.setFamily(("NSimSun"))
        font.setPointSize(12)
        self.btnValid.setFont(font)
        self.btnValid.setStyleSheet(("color: rgb(255, 255, 255);\n"
"    border: 2px solid #555;\n"
"    border-radius: 50px;\n"
"    border-style: outset;\n"
"    \n"
"background-color: #16f45d;\n"
"    padding: 5px;"))
        self.btnValid.setObjectName(("btnValid"))
        self.lbnotification = QtWidgets.QLabel(self.centralwidget)
        self.lbnotification.move(100,180)
        font = QtGui.QFont()
        font.setFamily("PMingLiU-ExtB")
        font.setPointSize(26)
        font.setBold(True)
        font.setWeight(75)
        self.lbnotification.setFont(font)
        self.lbnotification.setStyleSheet("color: rgb(255, 255, 255);")
        self.lbnotification.setObjectName("lbnotification")
        self.imageia1 = QtWidgets.QLabel(self.centralwidget)
        self.imageia1.setGeometry(QtCore.QRect(590, 190, 521, 391))
        self.imageia1.setScaledContents(True)
        font = QtGui.QFont()
        font.setFamily(("PMingLiU-ExtB"))
        font.setPointSize(26)
        font.setBold(True)
        font.setWeight(75)
        self.imageia1.setFont(font)
        self.imageia1.setStyleSheet(("color: rgb(255, 255, 255);\n"
"gridline-color: rgb(255, 255, 255);\n"
"border-color: rgb(255, 255, 255);"))
        self.imageia1.setText((""))
        self.imageia1.setObjectName(("imageia1"))
        self.imageia2 = QtWidgets.QLabel(self.centralwidget)
        self.imageia2.setGeometry(QtCore.QRect(1120, 190, 521, 391))
        self.imageia2.setScaledContents(True)
        font = QtGui.QFont()
        font.setFamily(("PMingLiU-ExtB"))
        font.setPointSize(26)
        font.setBold(True)
        font.setWeight(75)
        self.imageia2.setFont(font)
        self.imageia2.setStyleSheet(("color: rgb(255, 255, 255);"))
        self.imageia2.setText((""))
        
        self.imageia2.setObjectName(("imageia2"))
        self.imageia2_2 = QtWidgets.QLabel(self.centralwidget)
        self.imageia2_2.setGeometry(QtCore.QRect(600, 690, 301, 291))
        self.imageia2_2.setScaledContents(True)
        font = QtGui.QFont()
        font.setFamily(("PMingLiU-ExtB"))
        font.setPointSize(26)
        font.setBold(True)
        font.setWeight(75)
        self.imageia2_2.setFont(font)
        self.imageia2_2.setStyleSheet(("color: rgb(255, 255, 255);"))
        self.imageia2_2.setText((""))
        self.imageia2_2.setScaledContents(True)
        self.imageia2_2.setObjectName(("imageia2_2"))
        self.btnRestart = QtWidgets.QPushButton(self.centralwidget)
        self.btnRestart.move(100,50)
        self.btnRestart.setText("Recommencer")
        self.btnRestart.setStyleSheet("background-color : #16f45d; \nborder: 5px;\nborder-radius: 5px;\npadding: 20px;"
        "color: white;"
        )
        self.btnRestart.clicked.connect(self.gameController.btnRestart_click)
        self.btnRestart.show()
        self.btnplat1_2 = QtWidgets.QPushButton(self.centralwidget)
        self.btnplat1_2.setGeometry(QtCore.QRect(520, 130, 111, 101))
        self.btnplat1_2.setStyleSheet(("color: #333;\n"
"    border: 2px solid #555;\n"
"    border-radius: 50px;\n"
"    border-style: outset;\n"
"    background: qradialgradient(\n"
"        cx: 0.3, cy: -0.4, fx: 0.3, fy: -0.4,\n"
"        radius: 1.35, stop: 0 #fff, stop: 1 #888\n"
"        );\n"
"    padding: 5px;"))
        self.btnplat1_2.setObjectName(("btnplat1_2"))
        self.btnplat1_3 = QtWidgets.QPushButton(self.centralwidget)
        self.btnplat1_3.setGeometry(QtCore.QRect(1110, 120, 111, 101))
        self.btnplat1_3.setStyleSheet(("color: #333;\n"
"    border: 2px solid #555;\n"
"    border-radius: 50px;\n"
"    border-style: outset;\n"
"    background: qradialgradient(\n"
"        cx: 0.3, cy: -0.4, fx: 0.3, fy: -0.4,\n"
"        radius: 1.35, stop: 0 #fff, stop: 1 #888\n"
"        );\n"
"    padding: 5px;"))
        self.btnplat1_3.setObjectName(("btnplat1_3"))
        self.btnplat1_4 = QtWidgets.QPushButton(self.centralwidget)
        self.btnplat1_4.setGeometry(QtCore.QRect(580, 620, 111, 101))
        self.btnplat1_4.setStyleSheet(("color: #333;\n"
"    border: 2px solid #555;\n"
"    border-radius: 50px;\n"
"    border-style: outset;\n"
"    background: qradialgradient(\n"
"        cx: 0.3, cy: -0.4, fx: 0.3, fy: -0.4,\n"
"        radius: 1.35, stop: 0 #fff, stop: 1 #888\n"
"        );\n"
"    padding: 5px;"))
        self.btnplat1_4.setObjectName(("btnplat1_4"))
        
        self.boardMainWindow.setCentralWidget(self.centralwidget)
        self.btnValid.clicked.connect(self.gameController.btnvalid_click)
        self.tabBtn=[self.btnplat1,self.btnplat2,self.btnplat3,self.btnplat4,self.btnplat5,self.btnplat6,self.btnplat7,self.btnplat8,self.btnplat9]
        self.retranslateUi(self.boardMainWindow)
        QtCore.QMetaObject.connectSlotsByName(self.boardMainWindow)
        for btn in self.tabBtn:
            #btn.connectNotify(signal)
            pass
    def _define_color(self,pionChoisi):
        color = None
        if pionChoisi.couleur.name == "O" :
            #bleu
            color = "#252DFF"
        elif pionChoisi.couleur.name == "X":
            #orange
            color = "#FA8809"
        if color != None:
            self.tabBtn[pionChoisi.y*3 + pionChoisi.x].setStyleSheet("color: #333;\n"
                    "border: 2px solid #555;\n"
                    "border-radius: 50px;\n"
                    "border-style: outset;\n"
                    "background: qradialgradient(\n"
                    "        cx: 0.3, cy: -0.4, fx: 0.3, fy: -0.4,\n"
                    "        radius: 1.35, stop: 0 #fff, stop: 1 "+color+"\n"
                    "        );\n"
                    "padding: 5px;")  

    def clear(self):
        for btn in self.tabBtn:
            btn.setStyleSheet(self.dfStyle)

        #Horreur
        # self.btnplat1.setStyleSheet(self.dfStyle)
        # self.btnplat2.setStyleSheet(self.dfStyle)
        # self.btnplat3.setStyleSheet(self.dfStyle)
        # self.btnplat4.setStyleSheet(self.dfStyle)
        # self.btnplat5.setStyleSheet(self.dfStyle)         
        # self.btnplat6.setStyleSheet(self.dfStyle)
        # self.btnplat7.setStyleSheet(self.dfStyle)        
        # self.btnplat8.setStyleSheet(self.dfStyle)        
        # self.btnplat9.setStyleSheet(self.dfStyle)
    def updateLabelScore(self,score):
        self.lbscore_2.setText(f"score Joueur : {score[0]} score Robot : {score[1]}")
        self.lbscore_2.adjustSize()
        self.lbscore_2.show()   
    def updateLabelLose(self):
        self.lbnotification.setText("Vous avez perdu")
        self.lbnotification.adjustSize()

    def updateLabelWin(self):
        self.lbnotification.setText("Vous avez gagné")
        self.lbnotification.adjustSize()
    
    def updateLabelTie(self):
        self.lbnotification.setText("Egalité")
        self.lbnotification.adjustSize()

    def updateLabelAiTurn(self):
        self.lbnotification.setText("Au tour de l'IA")
        self.lbnotification.adjustSize()

    def updateLabelHumanTurn(self):
        self.lbnotification.setText("A vous de jouer")
        self.lbnotification.adjustSize()

    def updatePixMap(self):
        if(self.imageia1.isHidden() and self.imageia2.isHidden()):
            self.imageia2.show()
            self.imageia1.show()
        self.imageia1.setPixmap(QtGui.QPixmap(("saved.png")))
        self.imageia2.setPixmap(QtGui.QPixmap(("detectedShapes.jpg")))
           
    def retranslateUi(self, level):
        _translate = QtCore.QCoreApplication.translate
        self.boardMainWindow.setWindowTitle(_translate("level", "TIC TAC TOE"))
        self.btnValid.setText(_translate("level", "VALIDER"))

    def drawBoard(self,gameboard):
        img = PIL.Image.new('RGB', (400, 400), color = (73, 109, 137))
        fnt = PIL.ImageFont.truetype('Roboto-Bold.ttf', 50)
        d = PIL.ImageDraw.Draw(img)   
        d.text((20,20),gameboard.__str__(),font=fnt, fill=(255,255,0))
        img.save('plateau.png')
        if self.imageia2_2.isHidden():
            self.imageia2_2.show()
        self.imageia2_2.setPixmap(QtGui.QPixmap(("plateau.png")))
            
            
    def quitApp(self):
        self.dobot.disconnectDobot()
        QtWidgets.QApplication.quit()

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    level = QtWidgets.QMainWindow()
    ui = Ui_board()
    ui.setupUi(level)
    level.show()
    sys.exit(app.exec_())
