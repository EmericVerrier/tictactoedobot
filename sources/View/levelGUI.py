# -*- coding: utf-8 -*-

from PyQt5 import QtCore, QtGui, QtWidgets
import enum
import sources.Controller.AppController
import sources.Controller.GameController



class Ui_level(object):
    class GameLevel(enum.Enum):
        LEVEL_UNDEFINED = -1
        LEVEL_EASY = 0
        LEVEL_MEDIUM = 1
        LEVEL_HARD = 2

    def __init__(self,appController):
        self.appController = appController
        self.gameController = sources.Controller.GameController.GameController(appController,self)
        self.levelMainWindow = QtWidgets.QMainWindow()
        self.levelMainWindow.setObjectName("level")
        self.levelMainWindow.resize(1164, 773)
        self.levelMainWindow.setMaximumSize(QtCore.QSize(1164, 2581))
        self.levelMainWindow.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("plateauSimple.jpg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.levelMainWindow.setWindowIcon(icon)
        self.levelMainWindow.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.levelMainWindow.setStyleSheet("background-color: rgb(0, 85, 127);")
        self.centralwidget = QtWidgets.QWidget(self.levelMainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.btnEasy = QtWidgets.QPushButton(self.centralwidget)
        self.btnEasy.setGeometry(QtCore.QRect(110, 460, 261, 111))
        font = QtGui.QFont()
        
        font.setFamily("Bahnschrift Light Condensed")
        font.setPointSize(20)
        font.setBold(True)
        font.setWeight(75)
        self.btnEasy.setFont(font)
        self.btnEasy.setAutoFillBackground(False)
        self.btnEasy.setStyleSheet("color: #333;\n"
"    border: 2px solid #555;\n"
"    border-radius: 50px;\n"
"    border-style: outset;\n"
"    background: qradialgradient(\n"
"        cx: 0.3, cy: -0.4, fx: 0.3, fy: -0.4,\n"
"        radius: 1.35, stop: 0 #fff, stop: 1 #888\n"
"        );\n"
"    padding: 5px;")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("unnamed.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btnEasy.setIcon(icon1)
        self.btnEasy.setIconSize(QtCore.QSize(50, 50))
        self.btnEasy.setObjectName("btnEasy")
        self.logo = QtWidgets.QLabel(self.centralwidget)
        self.logo.setGeometry(QtCore.QRect(470, 190, 221, 201))
        self.logo.setMaximumSize(QtCore.QSize(5000, 5000))
        self.logo.setText("")
        self.logo.setPixmap(QtGui.QPixmap("plateauSimple.png"))
        self.logo.setScaledContents(True)
        self.logo.setObjectName("logo")
        self.lbhome = QtWidgets.QLabel(self.centralwidget)
        self.lbhome.setGeometry(QtCore.QRect(370, 50, 500, 111))
        font = QtGui.QFont()
        font.setFamily("PMingLiU-ExtB")
        font.setPointSize(26)
        font.setBold(True)
        font.setWeight(75)
        self.lbhome.setFont(font)
        self.lbhome.setStyleSheet("color: rgb(255, 255, 255);")
        self.lbhome.setObjectName("lbhome")
        self.btnDiff = QtWidgets.QPushButton(self.centralwidget)
        self.btnDiff.setGeometry(QtCore.QRect(440, 460, 291, 111))
        font = QtGui.QFont()
        font.setFamily("Bahnschrift Light Condensed")
        font.setPointSize(20)
        font.setBold(True)
        font.setWeight(75)
        self.btnDiff.setFont(font)
        self.btnDiff.setAutoFillBackground(False)
        self.btnDiff.setStyleSheet("color: #333;\n"
"    border: 2px solid #555;\n"
"    border-radius: 50px;\n"
"    border-style: outset;\n"
"    background: qradialgradient(\n"
"        cx: 0.3, cy: -0.4, fx: 0.3, fy: -0.4,\n"
"        radius: 1.35, stop: 0 #fff, stop: 1 #888\n"
"        );\n"
"    padding: 5px;")
        self.btnDiff.setIcon(icon1)
        self.btnDiff.setIconSize(QtCore.QSize(50, 50))
        self.btnDiff.setObjectName("btnDiff")
        self.btnExpert = QtWidgets.QPushButton(self.centralwidget)
        self.btnExpert.setGeometry(QtCore.QRect(790, 460, 281, 111))
        font = QtGui.QFont()
        font.setFamily("Bahnschrift Light Condensed")
        font.setPointSize(20)
        font.setBold(True)
        font.setWeight(75)
        self.btnExpert.setFont(font)
        self.btnExpert.setAutoFillBackground(False)
        
        self.btnExpert.setStyleSheet("color: #333;\n"
"    border: 2px solid #555;\n"
"    border-radius: 50px;\n"
"    border-style: outset;\n"
"    background: qradialgradient(\n"
"        cx: 0.3, cy: -0.4, fx: 0.3, fy: -0.4,\n"
"        radius: 1.35, stop: 0 #fff, stop: 1 #888\n"
"        );\n"
"    padding: 5px;")
        self.btnExpert.setIcon(icon1)
        self.btnExpert.setIconSize(QtCore.QSize(50, 50))
        self.btnExpert.setObjectName("btnExpert")
        self.btnExpert.clicked.connect(self.gameController.btnExpert_click)
        self.btnDiff.clicked.connect(self.gameController.btnMedium_click)
        self.btnEasy.clicked.connect(self.gameController.btnEasy_click)
        self.levelMainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(self.levelMainWindow)
        self.statusbar.setObjectName("statusbar")
        self.levelMainWindow.setStatusBar(self.statusbar)
        self.retranslateUi(self.levelMainWindow)
        QtCore.QMetaObject.connectSlotsByName(self.levelMainWindow)

    def retranslateUi(self, level):
        _translate = QtCore.QCoreApplication.translate
        self.levelMainWindow.setWindowTitle(_translate("level", "TIC TAC TOE"))
        self.btnEasy.setText(_translate("level", "FACILE"))
        self.lbhome.setText(_translate("level", "Choisissez votre niveau"))
        self.btnDiff.setText(_translate("level", "DIFFICILE"))
        self.btnExpert.setText(_translate("level", "EXPERT"))
        
if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    level = QtWidgets.QMainWindow()
    ui = Ui_level()
    ui.setupUi(level)
    level.show()
    sys.exit(app.exec_())
