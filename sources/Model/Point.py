import sources.Model.Colors
class Point:
    def __init__(self, x, y,posx,posy):
        self.x = x
        self.y = y
        self.posx_plateau=posx
        self.posy_plateau=posy
        self.couleur=sources.Model.Colors.Colors._
    def __str__(self)->str:
        return self.couleur.name
    def isEmpty(self)->bool:
        if self.couleur == sources.Model.Colors.Colors._:
            return True
        else:
            return False
