import enum
class GameLevel(enum.Enum):
        LEVEL_UNDEFINED = -1
        LEVEL_EASY = 0
        LEVEL_MEDIUM = 1
        LEVEL_HARD = 2