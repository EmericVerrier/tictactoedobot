from .Point import Point
import sources.Model.Colors
class GameBoard(list):  
    def __init__(self):
        self.tabcords=[[Point(x=j,y=i,posx=0,posy=0) for j in range(3)]for i in range(3)]
    def isEmpty(self) -> bool:
        for ligne in self.tabcords:
            for case in ligne:
                if case.couleur.name != sources.Model.Colors.Colors._.name:                        
                    return False
        return True
    def __len__(self):
        longueurTableau = 0
        for line in self.tabcords:
            for pion in line:
                if pion.couleur != sources.Model.Colors.Colors._:
                    longueurTableau+=1
        return longueurTableau
    def __str__(self) -> str:
        result = str()
        for line in self.tabcords:
            for pawn in line:
                result = result + pawn.couleur.name
            result = result + "\n"          
        return result
    def __getitem__(self, i) -> Point:
        return self.tabcords[i//3][i%3]
    
    def __contains__(self, point:sources.Model.Point.Point):
        for line in self.tabcords:
            for pion in line:
                if pion.couleur == point.couleur and pion.x == point.x and pion.y == point.y:
                    return True
        return False

    def _erase(self,point:sources.Model.Point.Point):
        point.couleur = sources.Model.Colors.Colors._
    #retourne les positions non occupées par les joueurs
    def _get_all_free_pos(self):
        #problème, cette fonction peut renvoyer des listes vides malgré la condition if... problème susceptible de faire planter make_best_move
        #free = [[j for j in i if j.couleur.name == sources.Model.Colors.Colors._.name] for i in self.tabcords if len(i) > 0]        
        free = [self[i] for i in range(9) if self[i].couleur == sources.Model.Colors.Colors._]
        return free