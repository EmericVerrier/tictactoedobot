import cv2
import numpy as np
from .GameBoard import GameBoard
from .MyCapture import MyCapture
from typing import List
import sources.Model.Colors, sources.Model.Point
from enum import Enum
class ImageRecognition(object):
    class Square(object):
        def __init__(self,posX : int, posY : int):
            self.positionX = posX
            self.positionY = posY
            self.area = int()
            self.color = sources.Model.Colors.Colors._
        def __str__(self)-> str:
            valueToReturn = str(f"{self.color.name} Square centered in : {self.positionX},{self.positionY}, area : {self.area}")
            return valueToReturn
    def __init__(self, uiNumCamera=0):
        self.image = np.zeros((480,640,3),np.uint8)
        self.capture = MyCapture(uiNumCamera)
        self.detectedSquares = [List[self.Square]]
        self.epsilonLevel = 0.0
        self.blurLevel = 1
        self.maxValCanny = 0
        self.minValCanny = 0

    ''' cette méthode permet de déterminer la couleur d'un point donné dans l'image en fonction de sa distance par rapport au bleu ou au orange
    la valeur de retour est soit orange, soit bleu ou gris'''
    def getColor(self,x,y) -> sources.Model.Colors.Colors:
        hsv = cv2.cvtColor(self.image, cv2.COLOR_BGR2HSV)
        h,s,v = cv2.split(hsv)
        SquaredDistanceHueToAzure = (115-h[y,x])*(115-h[y,x])
        SquaredDistanceHueToOrange = (15-h[y,x])*(15-h[y,x])
        if(SquaredDistanceHueToAzure < SquaredDistanceHueToOrange and v[y,x]>140):
            return sources.Model.Colors.Colors.O
        elif(v[y,x] > 140 and s[y,x] > 38):
            return sources.Model.Colors.Colors.X
        else:
            return sources.Model.Colors.Colors._

# load the image and resize it to a smaller factor so that
# the shapes can be approximated better

    ''' la fonction recognition s'appuie sur une capture du plateau de jeu qui a été prise auparavant
    pour reconnaître les carrés oranges et les carrés bleus du plateau, pour cela, plusieurs transformations seront appliquées 
    à l'image pour détecter les bords des formes ainsi que les couleurs, une fois ces transormations appliquées et les approximations des carrés faites,
    pour chaque approximation, une forme sera créée sur le plateau de d'après la couleur du contenu de cette forme'''
    def recognition(self,blurLevel=5,minValCanny=50,maxValCanny=75, epsilonLevel=0.1)-> sources.Model.GameBoard.GameBoard:
        self.image = self.capture.captureImage()
        if self.image is None:
            raise FileNotFoundError("image non trouvée")
        else:
            img_width = self.image.shape[1]
            img_height = self.image.shape[0]
            grayScale = cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY)

            imageBlurred = cv2.medianBlur(grayScale,5)
            edges = cv2.Canny(imageBlurred, 50,75)
            edges = cv2.dilate(edges, (5,5), iterations=3)
            edges = cv2.erode(edges, (3,3))
            shapesContours, hierarchies = cv2.findContours(edges, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_TC89_KCOS)
            i = 0
            plateau =sources.Model.GameBoard.GameBoard()
            for contour in shapesContours:
                approx = cv2.approxPolyDP(contour, 0.1 * cv2.arcLength(contour, True), True)
                cv2.drawContours(self.image, [contour], 0, (0, 0, 255), 1)
                if len(approx) == 4 and cv2.contourArea(approx) > 600:
                    moments = cv2.moments(contour)
                    if(moments['m00'] != 0):
                        cX =int(moments['m10']/moments['m00'])
                        cY = int(moments['m01']/moments['m00'])
                    x,y,w,h = cv2.boundingRect(approx)
                    cv2.rectangle(self.image, (x,y), (x+w,y+h), (255,0,0),2)
                    square = self.Square(cX, cY)
                    square.color = self.getColor(cX, cY)
                    square.area = cv2.contourArea(approx)
                    self.detectedSquares.append(square)
                    self.fillBoard(square.positionX, square.positionY, square.color,plateau)
                    cv2.putText(self.image, 'Quadrilateral : '+square.color.name,(x,y),cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 255, 255), 2)
            cv2.imwrite("detectedShapes.jpg", self.image)
            cv2.waitKey(0)
            cv2.destroyAllWindows()    
            return plateau

    def fillBoard(self,cX,cY,couleur:sources.Model.Colors.Colors, plateau:sources.Model.GameBoard.GameBoard = None):
        col=-1
        row=-1
        if cX>0 :
            if cX < 205 :
                col=0
            elif cX < 340 :
                col=1
            else:
                col=2

            if cY < 140 :
                row=0
            elif cY < 265 :
                row =1
            else :
                row=2
            p = sources.Model.Point.Point(col,row,cX,cY)
            p.couleur=couleur
            if(plateau != None and row >= 0 and col >= 0):
                plateau.tabcords[row][col] = p
            else:
                print("pion non retenu")