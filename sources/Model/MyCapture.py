import os
import cv2

class MyCapture(object):
    def __init__(self, uiIndex):
        self.numcam = uiIndex
        self.webcam = cv2.VideoCapture()
        if(uiIndex == None):
            raise AttributeError("numCam must be different of None")
        else:
            if(os.name == "posix"):
                self.webcam.open(uiIndex,cv2.CAP_V4L)
                
            else:
                self.webcam.open(uiIndex,cv2.CAP_DSHOW)
    ''' la fonction getWebcam est un accesseur en lecture sur l'attribut webcam, un objet VideoCapture'''

    ''' la fonction captureImage permet d'enregistrer des images captées par la webcam et 
    les affiche un court instant en leur passant un filtre noir et blanc'''
    def captureImage(self):
        if self.webcam.isOpened() == False:
            try:
                self.webcam.open(self.numcam,cv2.CAP_DSHOW)
            except AttributeError("ouverture de camera impossible"):
                print("ouverture de camera impossible")
        if self.webcam.isOpened():
            check, frame = self.webcam.read()
            if check == True:
                cv2.imwrite(filename='saved.png', img=frame)
                img_new = cv2.imread('saved.png')
                key = cv2.waitKey(5)
                self.webcam.release()
                return img_new
        return None
    def previewImage(self):
        try:
            if self.webcam.isOpened():
                while(True):
                    check, frame = self.webcam.read()
                    cv2.imshow('preview',frame) 
                    if cv2.waitKey(1) & 0xFF == ord('q'):
                        break
                self.webcam.release()
                cv2.destroyAllWindows()
            else:
                valueErr = ValueError("impossible d'ouvrir la caméra")
                raise valueErr
        except(KeyboardInterrupt):
            webcam.release()
            cv2.destroyAllWindows() 
        finally:
            return