import sources.View.mainGUI,sources.View.maintenanceGUI,sources.View.levelGUI, sources.View.calibrationGUI
import sources.Model.MyCapture, sources.Model.MyDobotControl
import sources.Model.ImageRecognition
from PyQt5.QtWidgets import QApplication
from cv2 import imshow, imread, waitKey, destroyAllWindows


import sys
class AppController:
    def __init__(self,runningApp : QApplication):
        self.runningApp = runningApp
        self.mainGUI = sources.View.mainGUI.Ui_MainWindow(self)
        self.dobot = sources.Model.MyDobotControl.MyDobot()
        self.uiNumCamera=0
    def btnMaint_click(self):
        self.maintenanceGUI = sources.View.maintenanceGUI.Ui_maintenance(self)
        self.uiNumCamera = self.maintenanceGUI.btnChangeCameraList.value()
        #self.dobot = MyDobotControl.MyDobot()
        self.maintenanceGUI.maintenanceMainWindow.show()

    def btnChangeCameraButtonClicked(self):
        cameraNulle = sources.Model.MyCapture.MyCapture(self.maintenanceGUI.btnChangeCameraList.value())
        self.uiNumCamera = self.maintenanceGUI.btnChangeCameraList.value()
        cameraNulle.previewImage()
    def btnCalibrationClicked(self):
        self.calibrationView = sources.View.calibrationGUI.calibrationGUI(self)
    def COMchanged(self, value):
        self.dobot.setCOM(str(value))

    def btnReturn_click(self):
        if (self.dobot.getDobotStatus() == True):
            self.dobot.disconnectDobot()        
        self.maintenanceGUI.maintenanceMainWindow.hide()
        
    def btnValiderPort_click(self):
        if (self.dobot.getDobotStatus() == True):
            self.dobot.disconnectDobot()
        self.dobot.connectDobot()
        self.maintenanceGUI.updateStateDobot(self.dobot.initDobot())

    def btnChangeCameraListValueChanged(self, value):
        self.uiNumCamera = value

    def btnTestAnalyse_click(self):
        recognitionObj = sources.Model.ImageRecognition.ImageRecognition(self.uiNumCamera)
        board = recognitionObj.recognition()
        imshow("detected Shapes", imread("detectedShapes.jpg"))
        print(f"{board} :  est le tableau instancié")
        waitKey()
        destroyAllWindows()
                

    def btnHomDobot_click(self):
        self.dobot.HOMDobot()
        
    def btnTestFiab_click(self):
        for x in range (2):
            self.dobot.MaintenanceMovements()
            self.maintenanceGUI.updateMovementDobot(x+1)
            print (x+1)
     
    def btnTestCamera_click(self):
        camera = sources.Model.MyCapture.MyCapture(self.uiNumCamera)
        camera.previewImage()

    def btnPlay_click(self):
        self.levelGUI  = sources.View.levelGUI.Ui_level(self)
        self.levelGUI.levelMainWindow.show()