import sources.View.boardGUI, sources.View.levelGUI
import sources.Controller.AppController
import sources.Model.Point, sources.Model.GameLevel ,sources.Model.ImageRecognition, sources.Model.GameBoard, sources.Model.Players, sources.Model.Colors
from random import randint
import random
from time import sleep
class GameController:
    def __init__(self,appController,levelGUI):
        self.AppController = appController
        self.levelGUI = levelGUI
        self.dobot = self.AppController.dobot
        self._winning_combinations = (
        [0, 1, 2], [3, 4, 5], [6, 7, 8],
        [0, 3, 6], [1, 4, 7], [2, 5, 8],
        [0, 4, 8], [2, 4, 6])

    def changePawnOnClick(self,event):
        print(event)
    def btnvalid_click(self,event):
        if(self.end):
            self.dobot.resetRound()
        self.checked=False
        self.AppController.runningApp.processEvents()         
        if self.end==False :
            """Création d'un plateau de jeu temporaire, à comparer avec le plateau de jeu enregistré pour la partie"""
            self.recognition = sources.Model.ImageRecognition.ImageRecognition(self.AppController.uiNumCamera)
            temporaryGameBoard = self.recognition.recognition()
            for ligne in temporaryGameBoard.tabcords:
                for nouveauPoint in ligne:
                    if nouveauPoint not in self.gameboard:
                        self._update_board(nouveauPoint, self.player)
                        self.boardGUI._define_color(nouveauPoint)
                        self.boardGUI.drawBoard(self.gameboard)
            self.boardGUI.updateLabelAiTurn()
            pionChoisi = self._ai_make_move(event)
            self.boardGUI._define_color(pionChoisi)
            self.boardGUI.drawBoard(self.gameboard)
            self.boardGUI.updatePixMap()
            win=self._winner()    
            if win==False:
                self.boardGUI.updateLabelHumanTurn()
                self.currentplayer = self.player
        self.AppController.runningApp.processEvents()  
        print(self.gameboard)
        
    def start(self,leveln):
        self.player = sources.Model.Players.Player()
        self.enemy = sources.Model.Players.Player()
        self.enemy.couleurJoueur = sources.Model.Colors.Colors.X
        self.player.couleurJoueur = sources.Model.Colors.Colors.O
        self.boardGUI = sources.View.boardGUI.Ui_board(self)
        self.gameboard = sources.Model.GameBoard.GameBoard()
        self.leveln = leveln
        self.checked=False
        self.score_ia = 0
        self.score_player=0
        self.end=False
        self.boardGUI.boardMainWindow.showMaximized()
        
    def quitApp(self):
        self.AppController.runningApp.quit()
    
    def btnRestart_click(self):
        self.boardGUI.updateLabelHumanTurn()
        self.boardGUI.clear()
        self.boardGUI.imageia1.hide()
        self.boardGUI.imageia2.hide()
        self.boardGUI.imageia2_2.hide()
        self.gameboard = sources.Model.GameBoard.GameBoard()
        self.end = False
        self.boardGUI.btnValid.show()
    def btnEasy_click(self):
        self.level = sources.Model.GameLevel.GameLevel.LEVEL_EASY
        self.start(self.level)
        
    def btnExpert_click(self):
        self.level = sources.Model.GameLevel.GameLevel.LEVEL_HARD
        self.start(self.level)
        
    def btnMedium_click(self):
        self.level = sources.Model.GameLevel.GameLevel.LEVEL_MEDIUM
        self.start(self.level)
    
    def _ai_make_move(self,event) -> sources.Model.Point.Point:
        self.currentplayer = self.enemy
        pionChoisi = self.make_best_move(self.gameboard,self.enemy,self.level)
        self._update_board(pionChoisi, self.enemy)
        self.dobot.movePawnTo('p'+str(pionChoisi.x+1),0)
        sleep(1)
        return pionChoisi
    #Mise à jour du plateau par les données du joueur
    #le tableau ne peut être mis à jour que de sorte à ce qu'une nouvelle case soit ajoutée
    #les anciennes cases déjà remplies ne doivent pas être écrasées
    def _update_board(self, case, joueur):
        if self.gameboard.tabcords[case.y][case.x].isEmpty():
            case.couleur = joueur.couleurJoueur
            self.gameboard.tabcords[case.y][case.x] = case
        return self.gameboard
        #self._gameboard.positions[pos].draw_symbol_on_position(player, pos)
    
    def make_best_move(self, board:sources.Model.GameBoard.GameBoard, player : sources.Model.Players.Player,difficulty : sources.Model.GameLevel.GameLevel)-> sources.Model.Point.Point:
        diff_random = 0
        if difficulty.value == 0:
            diff_random = 25
        elif difficulty.value == 1:
            diff_random = 75
        elif difficulty.value == 2:
            diff_random = 100
        
        # Generate random
        rnum = randint(0, 100)
        # Find available moves
        initValue = 50
        best_choices = []

        available_pos = self.gameboard._get_all_free_pos()
        if self.gameboard.isEmpty() and diff_random == 100:
            return sources.Model.Point.Point(0, 1, 0, 0)
        if rnum > diff_random and len(available_pos)>0:
            randomMove = random.choice(available_pos)
            return randomMove

        else:
            if player.couleurJoueur.name == "O":
                for move in available_pos:
                    self._update_board(move, player)
                    moveVal = self.minimax(board, self.enemy)
                    board._erase(move)
                    if moveVal > initValue:
                        best_choices = [move]
                        return move
                    elif moveVal == initValue:
                        best_choices.append(move)
            else:
                for move in available_pos:
                    self._update_board(move, player)
                    moveVal = self.minimax(board, self.player)
                    board._erase(move)

                    if moveVal < initValue:
                        best_choices = [move]
                        return move
                    elif moveVal == initValue:
                        best_choices.append(move)

            if len(best_choices)>0:
                return random.choice(best_choices)
            elif len(available_pos)>0:
                return random.choice(available_pos)
        

    def _winner(self) :
        winner=self._is_game_won()

        if (winner == "tie"):
            self.boardGUI.lbnotification.setText("TIE")
            self.boardGUI.lbnotification.adjustSize()

            # self.boardGUI.btnValid.setText("RECOMMENCER")
            self.end=True
            
            self.AppController.runningApp.processEvents()
            self.boardGUI.btnValid.hide()
            
        elif (winner == self.player.couleurJoueur.name):
            self.setPoints()
            self.boardGUI.lbnotification.setText("Vous avez gagné")
            self.boardGUI.lbnotification.adjustSize()

            # self.boardGUI.btnValid.setText("RECOMMENCER")
            self.end=True
            
            self.AppController.runningApp.processEvents()
            self.boardGUI.btnValid.hide()
        
        elif (winner == self.enemy.couleurJoueur.name):
            self.setPoints()
            
            self.boardGUI.lbnotification.setText("Vous avez perdu")
            self.boardGUI.lbnotification.adjustSize()
            #self.boardGUI.btnValid.setText("RECOMMENCER")
            self.end=True
            self.AppController.runningApp.processEvents()
            self.boardGUI.btnValid.hide()

        return self.end

    #vérifie s'il y'a un gagnant        
    def winner(self):
        return self._is_game_won()
    #Vérifie si le jeu est terminé sans vainqueur
    def _is_game_won(self):

        for player in [sources.Model.Colors.Colors.X.name,sources.Model.Colors.Colors.O.name]:
            for combos in self._winning_combinations:
                if (self.gameboard[combos[0]].couleur.name == player and self.gameboard[combos[1]].couleur.name == player and self.gameboard[combos[2]].couleur.name == player):
                    return player
                
        if "_" not in self.gameboard.__str__():
            return "tie"
        return None
            
    def quitApp(self):
        self.dobot.disconnectDobot()
        self.AppController.runningApp.quit()
        

    """TODO faire le lien entre la fonction minimax et  ai_make_best_move et adapter minimax au programme"""
    def minimax(self, newBoard, player:sources.Model.Players.Player):
        available_pos = self.gameboard._get_all_free_pos()
        if self._is_game_won_player(self.enemy, newBoard):
            score = 0
            return score
        elif self._is_game_won_player(self.player, newBoard):
            score = 100
            return score
        elif len(available_pos) == 0:
            score = 50
            return score
            
        if player.couleurJoueur.name == "O":
            bestVal = 0
            for var in available_pos:
                #comparaison de toutes les valeurs des mouvements en itérant sur chaque mouvement possible, l'algorithme procède par appel récursif jusqu'à arriver à une situation de fin
                # de partie
                newBoard = self._update_board(var,player)
                moveVal = self.minimax(newBoard, self.enemy)
                newBoard._erase(var)
                bestVal = max(bestVal, moveVal)
            return bestVal


        if player.couleurJoueur.name == "X":
            bestVal = 100
            for var in available_pos:
                # print("Making move: " + str(var))
                newBoard = self._update_board(var, player)
                moveVal = self.minimax(newBoard, self.player)
                newBoard._erase(var)
                bestVal = min(bestVal, moveVal)
            return bestVal



    def setPoints(self):
        available_pos = self.gameboard._get_all_free_pos()
        if self._is_game_won_player(self.enemy, self.gameboard):
            self.score_ia=self.score_ia + 100
            
           
        elif self._is_game_won_player(self.player, self.gameboard):
            self.score_player=self.score_player + 100
            
        elif len(available_pos) == 0:
            self.score_ia = self.score_ia +0
            self.score_player=self.score_player + 0
        self.boardGUI.updateLabelScore((self.score_player,self.score_ia))
    #Vérifie le joueur a gagné ou pas
    def _is_game_won_player(self, player, board):
        for combos in self._winning_combinations:
            if (self.gameboard[combos[0]].couleur.name == player.couleurJoueur.name and self.gameboard[combos[1]].couleur.name == player.couleurJoueur.name and self.gameboard[combos[2]].couleur.name == player.couleurJoueur.name):
                return True
        return False